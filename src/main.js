let socket;
let whiteboard;
let context;
let clickX = [];
let clickY = [];
let clickDrag = [];
let paint;
let lineColor;
const canvasWidth = '1024';
const canvasHeight = '1024';


function intialiseWhiteboard() {
    let whiteboardContainer = document.getElementById('whiteboardContainer');
    whiteboard = document.createElement('canvas');
    whiteboard.setAttribute('width', canvasWidth);
    whiteboard.setAttribute('height', canvasHeight);
    whiteboard.setAttribute('id', 'whiteboard');
    whiteboardContainer.appendChild(whiteboard);
    context = whiteboard.getContext("2d");

    whiteboard.addEventListener('mousedown', function (e) {
        paint = true;
        addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop);
        redraw(clickX, clickY, lineColor, true);
    });

    whiteboard.addEventListener('mousemove', function (e) {
        if (paint) {
            addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, true);
            redraw(clickX, clickY, lineColor, true);
        }
    });
    whiteboard.addEventListener('mouseup', function (e) {
        paint = false;
    });

    whiteboard.addEventListener('mouseleave', function (e) {
        paint = false;
    });
}


function addClick(x, y, dragging) {
    clickX.push(x);
    clickY.push(y);
    clickDrag.push(dragging);
}

function redraw(x, y, color, shouldEmit) {
    context.clearRect(0, 0, context.canvas.width, context.canvas.height);
    context.strokeStyle = color;
    context.lineJoin = "round";
    context.lineWidth = 5;
    for (let i = 0; i < x.length; i++) {
        context.beginPath();
        if (clickDrag[i] && i) {
            context.moveTo(x[i - 1], y[i - 1]);
        } else {
            context.moveTo(x[i] - 1, y[i]);
        }
        context.lineTo(x[i], y[i]);
        context.closePath();
        context.stroke();
    }
    if (shouldEmit) {
        socket.emit('screen_update', {'x': x, 'y': y});
    }
}

function setUsers(users){
    let userString = "";
    for(let i in users){
        userString+=users[i]+"   ";
    }
    let userDiv = document.getElementById("users");
    userDiv.innerHTML = userString
}

function drawOfOtherPlayer(response) {
    var board_data = response.board_state;
    var users  = response.users;
    setUsers(users);
    for(let client_id in data){
        let otherPlayerColor = "green";
        for (let j =0; j<board_data[client_id].data.length; j++){
            let otherClickX = board_data[client_id].data[j].x;
            let otherClickY = board_data[client_id].data[j].y;
            redraw(otherClickX, otherClickY, otherPlayerColor, false);
        }
    }

}


function setColor(data) {
    lineColor = data.color;
}

(function () {
    socket = io('http://192.168.0.16:8080');
    lineColor = 'red';
    socket.emit('init', {'name': 'test'});
    socket.on('set', setColor);
    socket.on('screen_update', drawOfOtherPlayer);
    intialiseWhiteboard();
}());
